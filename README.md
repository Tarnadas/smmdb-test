# SMMDB Test

Short test to use [smmdb-lib](https://github.com/Tarnadas/smmdb-lib) in Node.

## Instructions

```sh
yarn install
yarn start
```

This should parse the test asset and print its information as JavaScript object.

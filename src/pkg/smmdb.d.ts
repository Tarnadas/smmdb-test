/* tslint:disable */
/* eslint-disable */
/**
*/
export function run(): void;
/**
* Super Mario Maker course file.
*
* This struct is a wrapper to hold a [SMMCourse](crate::proto::SMMCourse) struct which can be serialized via Protocol Buffer.
*/
export class Course {
  free(): void;
/**
* @param {Uint8Array} buffer
* @returns {Course}
*/
  static from_proto(buffer: Uint8Array): Course;
/**
* @param {Uint8Array} buffer
* @returns {Course}
*/
  static from_boxed_proto(buffer: Uint8Array): Course;
/**
* @param {any} course
* @returns {Course}
*/
  static from_js(course: any): Course;
/**
* @param {Uint8Array} buffer
* @returns {any[]}
*/
  static from_packed_js(buffer: Uint8Array): any[];
/**
* @returns {Uint8Array}
*/
  into_proto(): Uint8Array;
/**
* @returns {any}
*/
  into_js(): any;
}
/**
*/
export class Course2 {
  free(): void;
/**
* @param {Uint8Array} buffer
* @param {Uint8Array | undefined} thumb
* @returns {Course2}
*/
  static from_proto(buffer: Uint8Array, thumb?: Uint8Array): Course2;
/**
* @param {Uint8Array} buffer
* @param {Uint8Array | undefined} thumb
* @returns {Course2}
*/
  static from_boxed_proto(buffer: Uint8Array, thumb?: Uint8Array): Course2;
/**
* @param {any} course
* @param {Uint8Array | undefined} thumb
* @returns {Course2}
*/
  static from_js(course: any, thumb?: Uint8Array): Course2;
/**
* @param {Uint8Array} buffer
* @returns {any[]}
*/
  static from_packed_js(buffer: Uint8Array): any[];
/**
* @returns {Uint8Array}
*/
  into_proto(): Uint8Array;
/**
* @returns {any}
*/
  into_js(): any;
/**
* @param {Uint8Array} course
*/
  static decrypt(course: Uint8Array): void;
/**
* @param {Uint8Array} course
* @returns {Uint8Array}
*/
  static encrypt(course: Uint8Array): Uint8Array;
}
/**
*/
export class Thumbnail2 {
  free(): void;
}

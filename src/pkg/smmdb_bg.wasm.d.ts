/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export function __wbg_course_free(a: number): void;
export function course_from_proto(a: number, b: number): number;
export function course_from_boxed_proto(a: number, b: number): number;
export function course_from_js(a: number): number;
export function course_from_packed_js(a: number, b: number, c: number): void;
export function course_into_proto(a: number, b: number): void;
export function course_into_js(a: number): number;
export function __wbg_course2_free(a: number): void;
export function course2_from_proto(a: number, b: number, c: number, d: number): number;
export function course2_from_boxed_proto(a: number, b: number, c: number, d: number): number;
export function course2_from_js(a: number, b: number, c: number): number;
export function course2_from_packed_js(a: number, b: number, c: number): void;
export function course2_into_proto(a: number, b: number): void;
export function course2_into_js(a: number): number;
export function course2_decrypt(a: number, b: number): void;
export function course2_encrypt(a: number, b: number, c: number): void;
export function __wbg_thumbnail2_free(a: number): void;
export function run(): void;
export function __wbindgen_malloc(a: number): number;
export function __wbindgen_realloc(a: number, b: number, c: number): number;
export function __wbindgen_free(a: number, b: number): void;
export function __wbindgen_exn_store(a: number): void;

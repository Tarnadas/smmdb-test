import { readFile as readFileCb } from "fs";
import { promisify } from "util";

import { Course2 } from "./pkg/smmdb.js";

const readFile = promisify(readFileCb);

async function main() {
  try {
    const file = await readFile("./assets/test_course.tar");
    const course = Course2.from_packed_js(file);
    console.log(course);
  } catch (err) {
    console.error(err)
  }
}

main();
